#include <stdio.h>
#include <stdlib.h>

typedef struct Element Element;
typedef struct List List;

struct Element{
    int nombre;
    Element* suivant;
};

struct List{
    Element* debut;
    Element* fin;
    int taille;
};

/*****INITIALISATION****/
List* initialisation(){

    List* Liste=malloc(sizeof(List));
    Liste->debut=NULL;
    Liste->fin=NULL;
    Liste->taille=0;
    return Liste;
}

/*****INSERTION LISTE VIDE*****/
void ins_liste_vide(List* Liste, int nvnombre){

    Element* nouveau=malloc(sizeof(Element));
    Liste->debut=nouveau;
    Liste->fin=nouveau;
    nouveau->suivant=NULL;
    nouveau->nombre=nvnombre;
    Liste->taille++;
}

/****INSERER DEBUT LISTE NON VIDE****/
void ins_debut_liste(List* Liste, int nvnombre){

    Element* nouveau=malloc(sizeof(Element));
    nouveau->suivant=Liste->debut;
    Liste->debut=nouveau;
    nouveau->nombre=nvnombre;
    Liste->taille++;
}

/****INSERER FIN LISTE****/
void ins_fin_liste(List* Liste, int nvnombre){

    Element* nouveau=malloc(sizeof(Element));

    if (Liste->debut==NULL)
    {
        ins_liste_vide(Liste, nvnombre);
    }
    else
    {

        Liste->fin->suivant=nouveau;
        Liste->fin=nouveau;
        nouveau->nombre=nvnombre;
        nouveau->suivant=NULL;
        Liste->taille++;
    }
    
}

/****INSERER A LA POSITION****/
void ins_dans_liste(List* Liste,int position, int nvnombre){

    Element* nouveau=malloc(sizeof(Element));
    nouveau->nombre=nvnombre;

    
    int i=1;
    Element* temp=malloc(sizeof(Element));
    temp=Liste->debut;
    for ( i = 1; i < position-1; i++)
    {
         temp=temp->suivant;
    }
        
    nouveau->suivant=temp->suivant;
    temp->suivant=nouveau;
    //nouveau->pred=temp si on a liste doublement chainée chainée
     Liste->taille++;   
    
    
}

/****SUPP AU DEBUT****/
void supp_debut_liste(List* Liste){
    
    if (!Liste->debut)
    {
        exit(EXIT_FAILURE);
    }

    else if (Liste->debut!= NULL)
    {
        Element* supprimer= Liste->debut;
        Liste->debut=Liste->debut->suivant;
        free(supprimer);//libère la mémoire
    }
    Liste->taille--; 
}

/****SUPP DANS LA LISTE****/
supp_dans_liste(List* Liste,int position){

    int i=1;
    Element* temp=malloc(sizeof(Element));
    temp=Liste->debut;
    for ( i = 1; i < position-1; i++)
    {
         temp=temp->suivant;
    }

    Element* supp=temp->suivant;   
    temp->suivant=temp->suivant->suivant;
    free(supp);
    Liste->taille--; 
}

/****AFFICHAGE LISTE****/
void affichage(List* Liste){
    Element* temp=malloc(sizeof(Element));
    temp=Liste->debut;
    while (temp!=NULL)
    {
        printf("%d ->", temp->nombre);
        temp=temp->suivant;

    } 
}


/****INSERER A LA POSITION****/
void detruire (List* Liste){ 
  while (Liste->taille != 0){ 
    supp_debut_liste (Liste); 
    }
} 


int main(){
    List* Liste1=initialisation();

    ins_liste_vide(Liste1, 5);

    ins_debut_liste(Liste1, 6);

    ins_fin_liste(Liste1, 7);

    ins_dans_liste(Liste1,2,9);

    //supp_debut_liste(Liste1);

    supp_dans_liste(Liste1, 3);

    detruire(Liste1);

    affichage(Liste1);
    
    return 0;
}


//figo(n:entier)

    //fibo(n)=fibo(n-1)
