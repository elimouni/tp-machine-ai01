#include <stdio.h>
#include <stdlib.h>

typedef struct noeud
{
    struct noeud *gauche;
    struct noeud *droit;
    int cle;
}noeud;

typedef struct noeud *arbre;

void addNode(arbre *a, int key)
{
    arbre B;
    arbre A = *a;

    arbre elem = malloc(sizeof(noeud));

    if(!elem)
        printf("NTM");

    elem->cle = key;
    elem->gauche = NULL;
    elem->droit = NULL;

    if(A)
    {

    do
    {
    B=A;

        if(key > A->cle )
        {
            A = A->droit;

                if(!A)
                {
                B->droit = elem;

                }
        }
        else
        {
            A = A->gauche;

                if(!A)
                {
                B->gauche = elem;

                }
        }
    }
    while(A);

    }
    else
    {
    *a=elem;
    }
}

int max(arbre A)
{
    arbre Tmp=A;
    while(Tmp->droit)
    {
       Tmp=Tmp->droit;
    }
    return(Tmp->cle);
}

int min(arbre A)
{
    arbre Tmp=A;
    while(Tmp->gauche)
    {
       Tmp=Tmp->gauche;
    }
    return(Tmp->cle);
}

arbre succ(arbre A, int a)
{
    if(!A)
    {
        return NULL;
    }

    if(A->cle==a)
    {
    return A;
    }
    if(a>A->cle)
    {

    if(!A->droit)
        {
        printf("NTM");
        return NULL;
        }
    return(succ(A->droit,a));
    }
    else
    {
    arbre Tmp=succ(A->gauche,a);
        if(Tmp)
        {
        return Tmp;
        }
        else
        {
        return A;
        }
    }
}

void suppression(arbre A, int key)
{
    arbre precedent=A;

    while(A && A->cle!=key)
    {
        precedent=A;

        if(A->cle<key)
        {
            A=A->droit;
        }
        else
        {
            A=A->gauche;
        }
    }




    if(!A->droit && !A->gauche)
    {
        if(precedent->gauche==A)
        {
            precedent->gauche=NULL;
        }
        if(precedent->droit==A)
        {
            precedent->droit=NULL;
        }
        free(A);
        return;
    }
    if(A->droit && !A->gauche)
    {
        if(precedent->gauche==A)
        {
            precedent->gauche=A->droit;
        }
        if(precedent->droit==A)
        {
            precedent->droit=A->droit;
        }


        free(A);
        return;
    }
    if(!A->droit && A->gauche)
    {
        if(precedent->gauche==A)
        {
            precedent->gauche=A->gauche;
        }
        if(precedent->droit==A)
        {
            precedent->droit=A->gauche;
        }
        free(A);
        return;
    }
    if(A->droit && A->gauche)
    {
        int successeur=min(A->droit);

        arbre cellChanger=A;


        while(A->cle!=successeur)
        {

            if(A->cle<successeur)
            {
                A=A->droit;
            }
            else
            {
                A=A->gauche;
            }
        }
        cellChanger->cle=successeur;
        cellChanger->droit=A->droit;


        free(A);






        return;
    }
}

void printTree(arbre tree)
{
    if(!tree) return;

    if(tree->gauche)  printTree(tree->gauche);

    printf("Cle = %d\n", tree->cle);

    if(tree->droit) printTree(tree->droit);
}

int main()
{

    arbre New=NULL;
    addNode(&New,5);
    addNode(&New,3);
    addNode(&New,4);
    addNode(&New,1);
    addNode(&New,7);
    addNode(&New,6);
    addNode(&New,8);
    addNode(&New,9);
    addNode(&New,10);
    addNode(&New,11);






    printTree(New);

    int maximum=max(New);
    int minimum=min(New);
    printf("\n %d \n", maximum);
    printf("\n %d \n", minimum);

    /*arbre successeur = succ(New, 4);
    if(successeur)
    {
    printf("\n %d \n", successeur->cle);
    }*/

    suppression(New, 7);

    printTree(New);




return;
}
