#include <stdio.h>
#include<stdlib.h>

/******************************************************************************

Type File en utilisant un tableau circulaire de max éléments. Seulement max-1 éléments seront utilisés dans la file.
L'espace mémoire du tableau est alloué dynamiquement, on prévoit donc un pointeur au niveau de la structure MFile.
On utilise également les entiers suivants :
New : référence la prochaine case où doit s’insérer le futur élément
Old : référence l'élément le plus ancien de la File
max : indique la taille maximale de la file. On aurait pu le déclarer comme une constante globale avec #define. Dans notre cas chaque instance de la file peut avoir sa taille maximale,
alors qu'au niveau de la deuxième solution, on aura la même taille maximale pour chaque instance de la file.

*******************************************************************************/

typedef struct _File{
  int* Tab;
  int New;
  int Old;
  int max;
}File;

File* creer_file (int max){
    File* F=malloc(sizeof(File));
    if (!F){
        return NULL;
    }
    F->Tab=malloc(max*sizeof(int));

    if ((!F->Tab)){
        free(F);
        return NULL;
    }
    
    //Initialisation de new et old
    F->New=0;
    F->Old=F->New;
    F->max=max;
    return F;
}

int non_vide(File *F){
    /****************************************************************************
     * cette fonction retourne :
     * -1 si la file n'est pas bien initialisée
     * 0 si la file est vide
     * 1 si la file est non vide contient au moins un élément 
     * *****************************************************************************/

    if (!F){
        return -1;
    }

    return(F->New!=F->Old);
}

int non_pleine(File *F){
     /****************************************************************************
     * cette fonction retourne :
     * -1 si la file n'est pas bien initialisée
     * 0 si la file est pleine : on ne peut plus ajouter encore un élément 
     * 1 si la file n'est pas pleine : il y a encore au moins un emplacement vide
     * *****************************************************************************/

    if (!F) {
        return -1;
    }

    return((F->Old)!=((F->New)+1)%F->max);
}

int enfiler(File*F, int e){
     /********************************************
     * la fonction emfiler une élément si la file a été bien initialisée et s'il y a de la place pour le faire.
     * elle retourne 
     * -1 si la file n'est pas bien initialisée
     * 0 si l'insertion n'a pas été faite car la file est pleine
     * 1 si l'ajout a été réalisé
     ********************************************/
    if (!F){
        return -1;
    }
    
    int a;
    a=non_pleine(F);
    if (a>0){
        F->Tab[F->New]=e;
        F->New=(F->New+1)%F->max;
    }
    return a;
}

int defiler(File*F, int*e){
    /********************************************
     * la fonction la file un élément si la file a été bien initialisée et s'il y a au moins un élément dans la file.
     * elle enregistre l'élément défilé dans l'emplacement mémoire référencé par le pointeur e.
     * elle renvoie :
     * -1 si la file n'est pas bien initialisée
     * 0 si l'insertion n'a pas été faite car la file est vide
     * 1 si l'opération a été effectuée
     ********************************************/

    if (!F){
        return -1;
    }
    
    int a;
    a=non_vide(F);
    if (a>0){
        *e=F->Tab[F->Old];
        F->Tab[F->Old]=0;
        F->Old=(F->Old+1)%F->max;
    }
    return a;
    
}

void Libere(File*F)
{
    /********************************************
     * la fonction permet de libérer la mémoire utilisée par la file
     ********************************************/
    if(F){
        free(F->Tab);
        free(F);
    }
}

void Afficher(File* F){
    for (int i=0 ; i<F->max ; i++){
        if (F->Tab[i] != 0){
            printf("%d | ", F->Tab[i]);
        }
        else printf(" | ");
    }
    printf("\n");
}

int main(){
    File * F = creer_file(5);
    int e;
    for (int i=0;i<F->max;i++){
        F->Tab[i]=0;
    }

    enfiler(F,12);
    enfiler(F,15);
    enfiler(F,25);
    enfiler(F,45);
    Afficher(F);

    
    defiler(F,&e);
    printf("On a defile %d\n",e);

    Afficher(F);

    Libere(F);
    return 0;
}