#include<stdio.h>
#include<stdlib.h>

typedef struct _Pile{
  int* Tab;
  int Sommet;
  int max;
}Pile;

Pile* creer_pile (int max){

    Pile*P=malloc(sizeof(Pile));
    if (!P)
    {
        return NULL;
    }

    P->Tab=malloc(max*sizeof(int));

    if (!P->Tab)
    {
        return NULL;
    }

    P->Sommet=-1;
    P->max=max;

    for (int i=0; i<max; i++){
        P->Tab[i]=0;
    }
    return P;
}

int Non_Vide(Pile* P){
    /****************************************************************************
     * cette fonction retourne :
     * -1 si la pile n'est pas bien initialisée
     * 0 si la pile est vide
     * 1 si la pile est non vide contient au moins un élément 
     * *****************************************************************************/
    if(!P){//veut dire que la pile n'existe pas
        return -1;
    }
    return (P->Sommet>-1);//si sommet vaut 0 ca veut dire qu'on a ajouté une élement 

}

int Non_Pleine(Pile*P){
    /****************************************************************************
     * cette fonction retourne :
     * -1 si la pile n'est pas bien initialisée
     * 0 si la pile est pleine : on ne peut plus ajouter encore un élément 
     * 1 si la pile n'est pas pleine : il y a encore au moins un emplacement vide
     * *****************************************************************************/

    if (!P)
    {
        return -1;
    }
    return(P->Sommet< P->max-1);
}

int Empiler(Pile*P, int e){
    /********************************************
     * la fonction empile un élément si la pile a été bien initialisée et s'il y a de la place pour le faire.
     * elle retourne 
     * -1 si la pile n'est pas bien initialisée
     * 0 si l'insertion n'a pas été faite car la pile est pleine
     * 1 si l'ajout a été réalisé
     ********************************************/
    int a=Non_Pleine(P);
    if (a>0)
    {
        P->Sommet++;
        P->Tab[P->Sommet]=e;
    } 
    return a;
}

int Depiler(Pile*P, int *e){
    /********************************************
     * la fonction dépile un élément si la pile a été bien initialisée et s'il y a au moins un élément dans la pile.
     * elle enregistre l'élément dépilé dans l'emplacement mémoire référencé par le pointeur e.
     * elle renvoie :
     * -1 si la pile n'est pas bien initialisée
     * 0 si l'insertion n'a pas été faite car la pile est vide
     * 1 si l'opération a été effectuée
     ********************************************/
    int a;
    a=Non_Vide(P);
    if (a>0)
    {
        *e=P->Tab[P->Sommet];
        P->Tab[P->Sommet]=NULL;
        P->Sommet--;
    }
    return a;
}

void Libere(Pile*P)
{
     /********************************************
     * la fonction permet de libérer la mémoire utilisée par la pile
     ********************************************/
    if(P)
    {
        free(P->Tab);
        free(P);
    }
}

void Afficher(Pile* P){
    for (int i=0 ; i<P->max ; i++){
        if (P->Tab[i] != 0){
            printf("%d | ", P->Tab[i]);
        }
        else printf(" | ");
    }
    printf("\n");
}

int main(){

    Pile* P=creer_pile(10);
    int* e;

    printf("%d\n", Non_Vide(P));

    printf("%d\n", Non_Pleine(P));

    Empiler(P, 5);
    Empiler(P, 6);
    Empiler(P, 7);
    Empiler(P, 8);

    Afficher(P);

    Depiler(P, e);

    Afficher(P);

    return 0;
}