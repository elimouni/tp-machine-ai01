#include <stdio.h>
#include <stdlib.h>
#include "pile.c"
#include "E:\UTC\GI\GI01\AI01\TD\TD03\files.c"
#define Max 100


//Exo 1)
typedef struct arbre{
    int nb_noeuds;
    int Pere[Max];
    int Info[Max];
}arbre;


//Exo2)
int pere(int x, arbre a){
    //Test si pas valide par rapport à la plage des indices possible
    if (x < 0 || x >= a.nb_noeuds){
        return -2;
    }
    else{
        return a.Pere[x];
    }
}//Complexité en O(1)


//Exo3)
Pile * fils(arbre a, int x){
    Pile * myPile;
    if (x<0 || x>=a.nb_noeuds){
        return NULL;
    }
    else{
        myPile = creer_pile(Max);
        for (int i=0 ; i<a.nb_noeuds; i++){
            if (a.Pere[i] == x){
                empiler(myPile,i);
            }
        }
        return myPile;
    }
}//Complexité en O(n)


//Exo 5)
typedef struct arbre_k{
    arbre_k * Pere;
    int info[Max];
    arbre_k * Frere;
    arbre_k * Fils;
}arbre_k;
//On réprésente ici en fait uniquement un fils par noeud, et on récupère les autres fils avec le pointeur frere bout à bout.


//Exo 6)
arbre_k * creer_cellule(){
    arbre_k * myCell = malloc(sizeof(arbre_k));
    if (!myCell){
        return NULL;
    }
    myCell->Pere = NULL;
    myCell->Fils = NULL;
    myCell->Frere = NULL;
    return myCell;
}//Complexité en O(1)


//Exo 7)
void Affiche_niveau(arbre_k * P){
    MFile A,B,C;
    int niveau = 0;
    if (P){
        A = *CreerFile(Max);
        B = *CreerFile(Max);
        Enfiler(&A,P);
        while (!file_vide(A)){
            printf("\n niveau %d : ",niveau);
            while(!file_vide(A)){
                arbre_k temp = defiler(A); //Ici défiler doit retourner un arbre_k et non un entier.
                printf("%d ",temp->val); //De même le type de temp bloque ici.
                Enfiler_tous_les_fils(B,temp);
            }
            niveau ++;
            C = A;
            A = B;
            B = C;
        }
    }
}//Complexite en O(n)