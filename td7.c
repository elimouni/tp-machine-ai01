#include <stdio.h>
#include <stdlib.h>
#include "../td6/td6.c"

typedef struct Noeud{
    int val;
    Noeud gauche;
    Noeud droite;
}Noeud;

void parcours_recur_infixe(Noeud * x){
    if (x){
        printf("%d",x->val);
        parcours_recur_infixe(x->gauche);
        parcours_recur_infixe(x->droite);
    }
}

void parcours_infixe_infixe(Noeud * x){
    if (x){
        parcours_infixe_infixe(x->gauche);
        printf("%d",x->val);
        parcours_infixe_infixe(x->droite);
    }
}

void parcours_postfixe_infixe(Noeud * x){
    if (x){
        parcours_psotfixe_infixe(x->gauche);
        parcours_postfixe_infixe(x->droite);
        printf("%d",x->val);        
    }
}

void parcours_iter_infixe(Noeud * x){
    Pile * P = creer_Pile(10);
    while (x || non_vide(P)){
        while (x){
            empiler(P,x);
            printf("%d",x->val);
            x = x->gauche;
        }
        Depiler(P,x->val);
        x = x->droite;
    }
}

void parcours_iter_postfixe(Noeud * x){
    int n;
    Pile * P = creer_Pile(10);
    while (x || non_vide(P)){
        while (x){
            Empiler(P,x);
            n = 0;
            x = x->gauche;
        }
        x = Depiler(P,n);
        if (n==0){
            Empiler(P,x);
            n = 1;
            x = x->droite;
        }
        else{
            printf("%d",x->val);
            x = NULL;
        }
    }
}







