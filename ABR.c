#include <stdio.h>
//tri 
//min

/***STRUCT***/
typedef struct Noeud{
    int valeur;
    struct Noeud* fils_gauche;
    struct Noeud* fils_droit;
}Noeud;

int compteur;
int* pointeur_compteur=&compteur;

Noeud* creer_noeud(int cle){
    Noeud* p_noeud= malloc(sizeof(Noeud));

    p_noeud->valeur=cle;
    p_noeud->fils_droit=NULL;
    p_noeud->fils_gauche=NULL;

    return p_noeud;
}

/*ABR_Insérer(T, z)
y := nil
x := racine[T]
Tant_que x <> nil faire
 y := x
Si clé[z] < clé[x] alors x := gauche[x]
                              sinon x := droit[x]
Pere[z] := y
Si y = nil alors racine[T] := z
  			   sinon si clé[z] < clé[y] alors gauche[y] := z
						   sinon droit[y] :=z 
*/

void inserer_noeud(Noeud* racine, int cle){
    Noeud* new;
    new=creer_noeud(cle);
    Noeud* tmp= malloc(sizeof(Noeud));
    Noeud* racine_2=malloc(sizeof(Noeud));
    racine_2=racine;
    while (racine!=NULL)
    {
        tmp=racine;
        if (new->valeur<racine->valeur)
        {
            racine=racine->fils_gauche;
        }
        else
        {
            racine=racine->fils_droit;
        }
    }
    if (new->valeur<tmp->valeur)
    {
        tmp->fils_gauche=new;
    }
    else
    {
        tmp->fils_droit=new;
    }
}

void Afficher_arbre(Noeud* racine){
    if (racine)
    {
        printf("%d->", racine->valeur);
        Afficher_arbre(racine->fils_gauche);
        //infixe (mettre printf ici)
        Afficher_arbre(racine->fils_droit);
        //postfixe
    }
}

Noeud* trouver_min(Noeud* racine){
    while (racine->fils_gauche!=NULL)
    {
        racine=racine->fils_gauche;
    }
    return racine;
}

Noeud* trouver_max(Noeud* racine){
    while (racine->fils_droit!=NULL)
    {
        racine=racine->fils_droit;
    }
    return racine;
}

Noeud* recherche_iteratif(Noeud* racine, int val){
    while (racine!=NULL && val!=racine->valeur)
    {
        if (val<racine->valeur)
        {
            racine=racine->fils_gauche;
        }
        else
        {
            racine=racine->fils_droit;
        }
        *pointeur_compteur=*pointeur_compteur+1;
        printf("%d\n", compteur);
    }
    return racine;
}


Noeud* recherche_recursif(Noeud* racine, int val){
    if (racine==NULL || val==racine->valeur)
    {
        return racine;
    }
    if (val<racine->valeur)
    {
        return recherche_recursif(racine->fils_gauche, val);
    }
    else
    {
        return recherche_recursif(racine->fils_droit, val);
    }
}

//Le successeur est le plus petit des plus grands
/*ABR_Successeur(x)
Si droit[x] <> nil alors retourner ABR_Minimum(droit[x])
y := pere[x]
Tant_que y <> nil et x = droit[y] faire
x := y
y := pere[y]
Retourner (y)
*/
/*Noeud* Successeur_noeud(Noeud* racine, Noeud* pred){
    Noeud* p=malloc(sizeof(Noeud));
    p=recherche_iteratif(racine, pred->valeur);
}*/





int main(){
    Noeud* racine=creer_noeud(10);
    Noeud* min=malloc(sizeof(Noeud));
    Noeud* recherche_ite=malloc(sizeof(Noeud));
    Noeud* recherche_Rec=malloc(sizeof(Noeud));
    inserer_noeud(racine,15);
    inserer_noeud(racine,3);
    inserer_noeud(racine,2);
    inserer_noeud(racine,1);
    Afficher_arbre(racine);

    /***Recherche du min***/
    min=trouver_min(racine);
    printf("Le minimum de l'arbre est:%d\n", min->valeur);

    /***Recherche d'un noeud dans l'arbre***/
    recherche_ite=recherche_iteratif(racine,1);
    if (recherche_ite==NULL)
    {
        printf("Le noeud n'est pas dans l'arbre");
    }
    else
    {
        printf("%d\n", recherche_ite->valeur);
        printf("Le noeud se trouve au niveau %d dans l'arbre",  compteur);
    }

    /***Recheche récursif d'un noeud dans l'arbre***/
    recherche_Rec=recherche_recursif(racine,1);
    if (recherche_Rec==NULL)
    {
        printf("Le noeud n'est pas dans l'arbre");
    }
    else
    {
        printf("%d\n", recherche_Rec->valeur);
    }
    
    return 0;
}